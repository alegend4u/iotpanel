from django.contrib import admin

# Register your models here.
from Panel.models import *

admin.site.register(Person)
admin.site.register(Place)
admin.site.register(Residence)
admin.site.register(Employee)
admin.site.register(Guest)
admin.site.register(Dba)
admin.site.register(Intern)
admin.site.register(Manager)
admin.site.register(GuardHelper)
admin.site.register(Office)
admin.site.register(Parking)
admin.site.register(Mess)
