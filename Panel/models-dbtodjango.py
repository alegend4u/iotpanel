from django.db import models
from django.utils.timezone import now


class User(models.Model):
    User_Name = models.CharField(max_length=30)
    First_Name = models.CharField(max_length=30)
    Last_Name = models.CharField(max_length=30)
    Is_at = models.ForeignKey('Place', on_delete=models.CASCADE)



class Place(models.Model):
    Dual_Type = models.BooleanField(default=False)
    Allowed_Users = models.ManyToManyField(User)


class Contact(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value = models.BigIntegerField()


class Employee(User):
    Salary = models.BigIntegerField()
    DOJ = models.DateField(default=now(), blank=True)
    W_Hours = models.IntegerField(default=6)
    Holidays_PM = models.IntegerField(default=0)
    Works_Under = models.ForeignKey(Manager, on_delete=models.CASCADE)


class Guest(User):
    Date_of_Visit = models.DateField(default=now(), blank=True)
    Description = models.TextField(max_length=255)


class DBA(User):
    Maintainence_Log = models.DateField(default=now(), blank=True)


class Intern(Employee):
    End_Date = models.DateField(default=now(), blank=True)
    Works_Under = models.ForeignKey(Employee, on_delete= models.CASCADE)


class Manager(Employee):
    Has_employees = models.ManyToManyField(Employee)
    Has_Intern = models.ManyToManyField(Intern)


class Guard_Helper(Employee):
    Type = models.BooleanField(default=True)


class Parking(Place):
    Active_Units = models.IntegerField(default=0)


class Office(Place):
    pass


class Mess(Place):
    pass


class Residence(Place):
    pass

