from django.db import models
from django.utils.timezone import now

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    user_name = models.CharField(max_length=127, blank=True, null=True)
    contacts = models.TextField(unique=True, blank=True, null=True)  # This field type is a guess.
    is_at = models.ForeignKey('Place', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'USER'

class Employee(models.Model):
    salary = models.IntegerField(blank=True, null=True)
    doj = models.DateField(blank=True, null=True)
    work_hours = models.FloatField(blank=True, null=True)
    holidays_pm = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EMPLOYEE'

class Place(models.Model):
    p_id = models.AutoField(primary_key=True)
