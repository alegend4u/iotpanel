
from django.db import models


class Place(models.Model):
    p_id = models.AutoField(primary_key=True)
    p_name = models.CharField(max_length=31, default='Unknown Place')
    dual_type = models.BooleanField(blank=False, default=False)
    def __str__(self):
        return self.p_name


class Mess(Place):
    user_count = models.IntegerField(blank=True, default=0)
    expenditure = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Mess"


class Office(Place):
    project = models.CharField(unique=True, max_length=31, blank=True, null=True)
    manager = models.ForeignKey('Manager', models.CASCADE, blank=True, null=True)
    def __str__(self):
        return self.project

    def save(self, *args, **kwargs):
        self.p_name = 'Office: ' + self.project
        super(Office, self).save(*args, **kwargs)


class Parking(Place):
    active_units = models.IntegerField(blank=True, null=True)


class Residence(Place):
    ac = models.BooleanField(blank=True, null=True)
    booked_by = models.ForeignKey('Person', models.CASCADE, blank=True, null=True)


class Person(models.Model):
    user_id = models.AutoField(primary_key=True)
    user_name = models.CharField(unique=True, max_length=127)
    is_at = models.ForeignKey(Place, models.DO_NOTHING, db_column='is_at', blank=True, null=True)
    contacts = models.CharField(max_length=30, blank=True, null=True)
    def __str__(self):
        return self.user_name


class Dba(Person):
    maintenance_log = models.DateTimeField()


class Employee(Person):
    salary = models.IntegerField()
    doj = models.DateField()
    work_hours = models.FloatField()
    holidays_pm = models.IntegerField(blank=True, null=True)
    works_under = models.ForeignKey('Manager', models.DO_NOTHING, db_column='works_under', blank=True, null=True)


class Guest(Person):
    visit_date = models.DateField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)


class GuardHelper(Employee):
    type = models.BooleanField()


class Intern(Employee):
    end_date = models.DateField(blank=True, null=True)


class Manager(Employee):
    type = models.CharField(max_length=31, blank=True, null=True)
    level = models.IntegerField()
