from django import forms
from .models import *
from django.db import connection
from IotPanel import settings
import psycopg2

db_info = settings.DATABASES['default']

# psycon = psycopg2.connect(
#     user = db_info.get('USER'),
#     database = db_info.get('NAME'),
#     password = db_info.get('PASSWORD'),
#     host = db_info.get('HOST'),
#     port = db_info.get('PORT')
# )

def get_columns(table):
    columns_query = """select column_name from information_schema.columns
    where table_name = """
    # cursor = psycon.cursor()
    cursor = connection.cursor()
    cursor.execute(columns_query + "'" + table + "'")
    records = cursor.fetchall()
    return records

tables = connection.introspection.table_names()[:-10]
names = []
for i in tables:
    tname = i.split('_')[-1].capitalize()
    names.append((i, tname))

class display(forms.Form):
    table = forms.ChoiceField(choices=names, label='Select Table')