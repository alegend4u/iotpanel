from django.shortcuts import render
from django.db import connection
from . import forms
from .models import *

# Create your views here.
def home(request):
    return render(request, "Panel/home.html")

def panel(request):
    data = ''
    form = forms.display()
    if request.method == "POST":
        form = forms.display(request.POST)
        if form.is_valid():
            name = form.cleaned_data['table']
            table_instance = connection.introspection.installed_models([name])
            table_instance = list(table_instance)
            print(table_instance[0])
            data = table_instance[0].objects.all()
        else:
            data = ''
            print('invalid')
    return render(request, 'Panel/panel.html', {'form':form, 'data':data})

def about(request):
    return render(request, "Panel/about.html")

def more(request):
    return render(request, "Panel/more.html")